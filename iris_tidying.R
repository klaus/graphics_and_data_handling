df <- tibble(
  color = c("blue", "black", "blue", "blue", "black"),
  value = 1:5)

df


iris_mod <- mutate(iris, id = seq_len(nrow(iris)))

iris_gathered <- iris_mod %>%
                  gather(key = "measure", value = "value",
                         Sepal.Length : Petal.Width)



iris_spread <- iris_gathered %>% spread(key = measure, value = value)
